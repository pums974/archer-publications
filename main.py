import pandas as pd
from bokeh.plotting import figure, show, output_notebook, output_file
from bokeh.themes import built_in_themes
from bokeh.io import curdoc, export_png
from bokeh.models import Label
import geckodriver_autoinstaller


TOOLTIPS = [
    ("Year", "@time"),
    ("type", "$name"),
    ("total", "@$name"),
]


COLORS = [
    '#00ff41',  # matrix green
    '#F5D300',  # yellow
    '#FE53BB',  # pink
    '#08F7FE',  # teal/cyan
]
    

def produce_data():
    """produce data
    
    TODO automatic
    """

    df = pd.DataFrame({'time': list(range(2004, 2023)),
                  'conferences': [0,0,0,0,0,0,2,3,3,4,3,2,5,5,7,14,1,5,0],
                       'papers': [0,1,1,2,0,1,1,1,2,2,1,2,2,5,2,4,8,2,5],
                       'thesis': [1,0,0,1,0,0,2,0,1,1,0,1,1,1,0,1,2,0,0],
                       'others': [0,1,2,0,0,0,1,0,0,0,0,0,0,0,1,1,2,0,0]})
    df = df.set_index("time")
    df = df[["others", "thesis", "papers", "conferences"]]
    
    tcum = df.cumsum(axis=0).add_suffix("_tcum")
    cum = df.cumsum(axis=0).cumsum(axis=1).add_suffix("_cum")
    df = df.assign(**{c: tcum[c] for c in tcum}, **{c: cum[c] for c in cum})

    return df


def prepare_figure(df):
    """prepare figure"""
    curdoc().theme = 'dark_minimal'
    p = figure(title="Publications",
               aspect_ratio=600/400, sizing_mode="scale_both",
               x_range=(df.index.min()-1, df.index.max()+7), y_range=(0, df.max().max()*1.1),
               toolbar_location="above", tooltips=TOOLTIPS,
               tools="pan,box_zoom,tap,wheel_zoom,reset",
               active_drag="box_zoom", active_tap="tap")
    p.yaxis.axis_label = "Total number of publications"
    p.yaxis.axis_label_text_font_size = "10pt"
    return p


def stack_plot(p, df, names):
    """main stack plot"""
    p.varea_stack(source=df, x='time',
                  stackers=[f"{name}_tcum" for name in names],
                  legend_label=names,
                  color=COLORS, alpha=0.2)


def lines_and_markers(p, df, names):
    """Add lines and markers with neon effect"""
    n_shades = 10
    diff_linewidth = 1.1
    diff_radius = 0.02
    alpha_values = [1,] + [0.3 / n_shades] * n_shades
    for column, color in zip(names, COLORS):
        for n, alpha in enumerate(alpha_values):
            p.line(x="time", y=f"{column}_cum", source=df,
                   line_width=1.2 + n * diff_linewidth,
                   color=color, alpha=alpha,
                   legend_label=column, name=column)
            p.circle(x="time", y=f"{column}_cum", source=df,
                     radius=0.15 + n * diff_radius,
                     color=color, alpha=alpha,
                     legend_label=column, name=column)


def labels(p, df, names):
    """Add label at the end of the lines"""
    for column, color in zip(names, COLORS):
        last_time = df.index[-1]
        last_val = df[f"{column}_tcum"][last_time]
        last_pos = df[f"{column}_cum"][last_time]
    
        labels = Label(text=f"{last_val} {column}",
                       x=last_time + 0.5, y=last_pos,
                       text_color=color, text_font_size="10pt", text_baseline="middle")
        p.add_layout(labels)


if __name__ == "__main__":

    # for producing the png, we need this
    geckodriver_autoinstaller.install()  # Check if the current version of geckodriver exists
                                         # and if it doesn't exist, download it automatically,
                                         # then add geckodriver to path     

    df = produce_data()
    names = [str(col)
             for col in df.columns
             if not col.endswith("cum")]

    p = prepare_figure(df)

    stack_plot(p, df, names)
    lines_and_markers(p, df, names)
    labels(p, df, names)
    
    p.legend.visible = False
    
    # output_notebook()
    output_file("public/index.html", title='Archer publications')
    export_png(p, filename="public/plot.png")

    show(p)
