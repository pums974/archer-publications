## How it works

1. The `.gitlab-ci.yaml` install the dependencies listed in `requirements.txt`
2. The `.gitlab-ci.yaml` calls `main.py`
    1. `main.py` creates some data and a Bokeh plot.
    2. the Bokeh plot is rendered into an HTML in `public/index.html`
    3. the rendered Bokeh plot is saved as an image in `pubic/plot.png`
3. Gitlab host the content `public` to the address [https://pums974.gitlab.io/archer-publications](https://pums974.gitlab.io/archer-publications)
4. the plot can be integrated into an other webpage with :
```html
            <iframe height="100%" src="https://pums974.gitlab.io/archer-publications/" style="border:0" width="100%">
                <!--In case the browser does not support iframe-->
                <img alt="Time evolution of publications with ARCHER"
                     src="https://pums974.gitlab.io/archer-publications/plot.png"
                     style="width: 90%; display: block; margin-left: auto; margin-right: auto;"/>
            </iframe>
```


